const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

class ListGenerate {
    constructor(arr) {
        this.arr = arr;
    }

    render() {
        const root = document.querySelector("#root");
        const newList = document.createElement("ul");
        this.arr.forEach((value) => {
            try {
                if (!value.name || !value.author || !value.price) {
                    if (!value.name) {
                        throw new ReferenceError(`name is not defined`);
                    }
                    if (!value.author) {
                        throw new ReferenceError(`author is not defined`);
                    }
                    if (!value.price) {
                        throw new ReferenceError(`price is not defined`);
                    }
                } else {
                    const newItem = document.createElement("li");
                    for (const itemElement in value) {
                        if (newItem.textContent !== "") {
                            newItem.innerHTML = `${newItem.innerHTML}<br>${itemElement}: ${value[itemElement]}`
                        } else {
                            newItem.innerHTML = `${itemElement}: ${value[itemElement]}`
                        }
                    }
                    newList.append(newItem);
                    root.append(newList);
                }
            } catch (e) {
                console.log(e.message);
            }
        })
    }
}

new ListGenerate(books).render();